/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chats;

import java.net.*;  
import java.io.*;  
import java.util.Scanner;

 public class Client_1 {
     
    static ServerSocket ss;
    static Socket s;
    static InputStreamReader din;
    static BufferedReader br;
    static PrintStream ps;
    static String str="";
    static Scanner sc;
    
    public static void main(String args[])throws Exception{  
    
        ss=new ServerSocket(3333);  
        s=ss.accept();  
        din=new InputStreamReader(s.getInputStream());  
        br=new BufferedReader(din);     
        ps=new PrintStream(s.getOutputStream());   
        sc=new Scanner(System.in);
        
        System.out.println("server has started ....");
        
        sendThread st = new sendThread();        
        st.start();
        
        while(!str.equalsIgnoreCase("stop")){
            
	str=br.readLine(); 
        System.out.println("client 2 says: "+str);
        }
        
        br.close(); 
	s.close();  
	ss.close();  
        
    }
   
    public static class sendThread extends Thread{
     
        public void run(){
            while(!str.equalsIgnoreCase("stop")){
                str=sc.nextLine(); 
                ps.println(str);
            }
     }
    }
 }  

